#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;
use FindBin;
use Pod::Usage;
use Bio::SeqIO;
use Data::Dumper; 
use Bio::Tools::GFF;
use Bio::SeqIO;
my $gff = "";
my $assembly = "";
my $fasta = "";
my $outdir = ""; 
my $prefix = "";
my $length_cutoff;
my $is_contig;
my $rename;
my $help;

my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters

    -gff        GFF3 file
    -assembly   fai file produce by samtools faidx
    -fasta 		prot fasta
    -prefix     prefix to replace
    -outdir     outdir
    -rename     Add prefix to seq_id
    -is_contig  
    -help
/;
GetOptions(
    'gff=s'    => \$gff,
    'assembly=s'    => \$assembly,
    'fasta=s'  => \$fasta,
    'prefix=s' => \$prefix,
    'outdir=s' => \$outdir,
    'is_contig=s'=> \$is_contig,
    'rename=s' => \$rename,
    'length_cutoff=i'=> \$length_cutoff,
    'help|h|?' => \$help
)  or pod2usage(-message => $usage);
if ($help) { pod2usage(-message => $usage); }

# Open file
my $in = new Bio::Tools::GFF(
    -file => $gff,
    -gff_version => 3
);
open(IN,$assembly);
my $in_fasta = new Bio::SeqIO(
    -file => $fasta,
    -format => 'fasta'
);

# Output file
my $outfasta = $outdir ."/".$outdir.".faa";
my $gff_4synvisio_file = $outdir ."/". $outdir."_synvisio.gff";
my $gff_file = $outdir ."/". $outdir.".gff";
my $bed = $outdir ."/".$outdir.".bed";
my $coord = $outdir ."/".$outdir."_chromosome.bed";
my $seqout = new Bio::SeqIO(
    -file => ">$outfasta",
    -format => 'fasta'
); 
my $gff_file_temp1 = $gff_file."_temp1";
my $gff_file_temp2 = $gff_file."_temp2";
open(GFF1,">$gff_file_temp1");
open(GFF2,">$gff_file_temp2");
open(BED,">$bed");
open(COORD,">$coord");
my %fasta;
while(my $seqobj = $in_fasta->next_seq){
    my $display_id = $seqobj->display_id; 
        $fasta{$display_id} = $seqobj; 
}
$in_fasta->close;
my %keep;
while(<IN>) {
    chomp;
    my ($seq_id,$end) = (split(/\t/,$_))[0,1];
    if ($is_contig eq "chromosome") {
        next if $end < $length_cutoff; 
        $keep{$seq_id} = 1;  
        $seq_id = $seq_id ."_". $prefix if $rename; 
        print COORD join("\t",$seq_id,1,$end),"\n";
    }
}
close COORD;
close IN;
my %exist;
my  $cpt = 0;
while(my $feature = $in->next_feature) {
    my $seq_id = $feature->seq_id;
    if ($keep{$seq_id}){
        if ($feature->primary_tag eq "mRNA") {
            my ($name) = $feature->get_tag_values("Name");
            my ($parent) = $feature->get_tag_values("Parent");
            if ($exist{$parent}){
                next;
            }
            else {
                $exist{$parent} = 1;
                my $real_name = $name; 
        
                if ($fasta{$name}){ 
                    my $seqobj = $fasta{$name};
                    my $real_seq_id = $seq_id; 
                    $seq_id = $seq_id."_".$prefix if $rename;
                    print GFF1 join("\t",$real_seq_id,$real_name,$feature->start,$feature->end),"\n"; 
                    print GFF2 join("\t",$seq_id,$real_name,$feature->start,$feature->end),"\n"; 
                    print BED join("\t",$seq_id,$feature->start,$feature->end,$name),"\n";
                    $seqout->write_seq($seqobj);
                }
            }
        }
    }
}
system("sort -V -k1,1 -k2,2 $gff_file_temp1 > $gff_file");
system("sort -V -k1,1 -k2,2 $gff_file_temp2 > $gff_4synvisio_file");
system("rm $gff_file_temp1");
system("rm $gff_file_temp2");
close BED;
close GFF1;
close GFF2;
$in->close;
$seqout->close;