#!/usr/bin/env python
from Bio import SeqIO

import sys

last_gene = None
transcript = []  
for record in SeqIO.parse(sys.argv[1], "fasta"):
    if record.id.find(".")==-1:
        gene = record.id
    else:
        gene = ".".join(record.id.split(".")[:-1])
    
    current_length = len(record)
    if last_gene is not None:
        if gene == last_gene:
            if previous_length < current_length: 
                transcript.append(record)  
                previous_length = current_length
        else:
            last_gene = gene
            transcript.append(record)  
            previous_length = current_length
    else:
        last_gene = gene 
        transcript.append(record)
        previous_length = current_length
SeqIO.write(transcript, sys.argv[2], "fasta") 