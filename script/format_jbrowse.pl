#!/usr/bin/perl
use Bio::Tools::GFF;
use Getopt::Long;
use FindBin;
use Pod::Usage;
my $help = "";
my $collinearity ="";
my $coordinate = "";
my $out = "";
my $prefix = "";
my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters

    -collinearity        GFF3 file
    -coordinate   fai file produce by samtools faidx
    -fasta 		prot fasta
    -prefix     prefix to replace
    -outdir     outdir
    -is_contig
    -help
/;
GetOptions(
    'collinearity=s'    => \$collinearity,
    'coordinate=s'    => \$coordinate, 
    'prefix=s' => \$prefix,
    'out=s' => \$out, 
    'help|h|?' => \$help
)  or pod2usage(-message => $usage);
if ($help) { pod2usage(-message => $usage); }


   
my %coordinate;
my $gff1 = new Bio::Tools::GFF(
    -file => ">$out",
    -gff_version => 3
); 


open(IN,$coordinate);

while(<IN>) {
    chomp;
    my ($seq_id,$name,$start,$end) = (split(/\t/,$_));
    $coordinate{$name} = {
        start => $start,
        end   => $end,
        seq_id => $seq_id
    };
}
close IN;
my %count;
open(IN,$collinearity);
my ($block_strand,$block_size);
my $cpt = 0;
my %blocks;
my %feature1;
my %start;
my %end;
my %ref;
my %feature2;
while(<IN>){
    chomp;
    if ($_ =~ /^## Alignment/) {
        my @data = (split(/\s/,$_));
        $block = $data[2];
        $block =~ s/://;
        $block_size = (split(/\=/,$data[5]))[1]; 
        $block_strand = $data[7] eq "plus" ? "+" : "-";  
    }
    elsif ($_ =~ /^#/) {
        next;
    }
    else {
        my @link  = (split(/\t/,$_));
         
        my $gene1 = $link[1];
        my $gene2 = $link[2];
        
        if ($coordinate{$gene1} && $coordinate{$gene2} ) {
            my $block_id1 = join("_",$prefix,$block); 
            my $part2 = $coordinate{$gene2}->{seq_id} .":".$coordinate{$gene2}->{start}."..".$coordinate{$gene2}->{end}; 
            my $feature_part1 = Bio::SeqFeature::Generic->new(
                -seq_id      => $coordinate{$gene1}->{seq_id},
                -primary_tag => "match_part",
                -source_tag  => $prefix,
                -start       => $coordinate{$gene1}->{start},
                -end         => $coordinate{$gene1}->{end},
                -strand      => $block_strand,
                -tag         => {
                    Parent   => $block_id1,
                    query     => $gene1,
                    target    => $gene2,
                    query_id  => $gene1,
                    target_id => $gene2, 
                    location  => $part2,
                    block_strand => $block_strand
                }
            );
            push @{$feature1{$coordinate{$gene1}->{seq_id}}{$block_id1}} , $feature_part1;
            if ($start{$coordinate{$gene1}->{seq_id}}{$block_id1}) {
                $start{$coordinate{$gene1}->{seq_id}}{$block_id1} = $coordinate{$gene2}->{start} if $coordinate{$gene2}->{start} < $start{$coordinate{$gene1}->{seq_id}}{$block_id1} ;
                $end{$coordinate{$gene1}->{seq_id}}{$block_id1}     = $coordinate{$gene2}->{end} if $coordinate{$gene2}->{end} > $end{$coordinate{$gene1}->{seq_id}}{$block_id1}  ; 
                
            }
            else {
                $start{$coordinate{$gene1}->{seq_id}}{$block_id1} = $coordinate{$gene2}->{start};
                $end{$coordinate{$gene1}->{seq_id}}{$block_id1}   = $coordinate{$gene2}->{end};
                $ref{$coordinate{$gene1}->{seq_id}}{$block_id1} =$coordinate{$gene2}->{seq_id};
            }
        } 
    }
}

foreach my $seq_id (sort {$a cmp $b} keys %feature1 )  {
    
    foreach my $block_id (sort {$a cmp $b} keys %{$feature1{$seq_id}}) {
        my @feature = sort {$a->start <=> $b->start} @{$feature1{$seq_id}{$block_id}};
        my $block_size = scalar(@feature);        
        my $part = $feature[0]->seq_id .":".$feature[0]->start."..".$feature[-1]->end;
        my $part2 =  $ref{$seq_id}{$block_id} .":".$start{$seq_id}{$block_id} ."..".$end{$seq_id}{$block_id};
        my $feature = Bio::SeqFeature::Generic->new(
            -seq_id      => $feature[0]->seq_id,
            -primary_tag => "match",
            -source_tag  => $prefix,
            -start       => $feature[0]->start,
            -end         => $feature[-1]->end,
            -strand      => $feature[0]->strand,
            -tag         => {
                ID   => $block_id,
                Name   => $block_id,
                Note => $part2,
                location => $part2,
                block_size => $block_size
            }
        );
        $gff1->write_feature($feature);
        foreach my $feature_part (@feature){
            $gff1->write_feature($feature_part);
        }
    }
}
$gff1->close;