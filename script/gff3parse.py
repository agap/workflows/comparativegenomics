#!/usr/bin/env python
from BCBio import GFF
import sys
import re

matches = ['scaffold','chloro','mito','Un']
file = open(sys.argv[1], "r")
for line in file:
    if not any( x in line for x in matches):
        x = line.strip().split()
        chr = sys.argv[2] + re.sub('\D', '', x[0])
        bed = [chr,1,x[1]]         
        print("\t".join(map(str, bed)))
  



#in_handle = open(sys.argv[1])
#for rec in GFF.parse(in_handle):
#    for feature in rec.features:
#        print(feature)
#        if feature.type == 'mRNA':
#            print(feature)