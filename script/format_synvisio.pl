#!/usr/bin/perl 
use Data::Dumper;
use Getopt::Long;
use YAML::Tiny;
use File::Basename;
my $help;
my $directory;
my $config;
my $sample;
my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters

    -directory   Directory containing synvisio result 
    -config      config.yaml need by Snakemake 
    -help
/;
GetOptions(
    'directory=s'    => \$directory,
    'config=s'    => \$config, 
    'help|h|?' => \$help
)  or pod2usage(-message => $usage);
if ($help) { pod2usage(-message => $usage); }

# Open the config
my $yaml = YAML::Tiny->read($config);  

my %species_real_name;
my $root = $yaml->[0]->{input_proteome};
foreach my $species (keys %$root) {
    my $prefix = $yaml->[0]->{input_proteome}->{$species}->{prefix};
    my $name   = $yaml->[0]->{input_proteome}->{$species}->{name};

    $species_real_name{$species} = {
        name => $name,
        prefix => $prefix
    };
}
use Cwd 'abs_path';
my $abs_path = abs_path($config); 

my($filename, $directories, $suffix) = fileparse($abs_path);
open(IN,"ls $directory/*gff|");
open(PAG,">$directory/Home.jsx");
open(CODE,">$directory/sampleSourceMapper.js");
print CODE "// the files need to be located in the files sub folder inside assets folder 
export default {\n";
while(<IN>){
    chomp;
    my $files = $_;
    if (-s $files) {
        $files =~ s/$directory\///;
        $files =~ s/_coordinate.gff//;
        next if $files eq "full_comparison";
        my ($species1,$species2) = (split(/\_/,$files));
        my $reverse = join("_",$species2,$species1); 
        my $name1 = $species_real_name{$species1}->{name}; 
        my $name2 = $species_real_name{$species2}->{name}; 
        my $bed1 = $directories."/".$species1."/".$species1."_chromosome.bed";
        my $bed2 = $directories."/".$species2."/".$species2."_chromosome.bed";
        my @output1 = `cut -f 1 $bed1 `;
        my @output2 = `cut -f 1 $bed2 `;
        chomp @output1;
        chomp @output2;
    
        print CODE "    '$files': {\n";
        print CODE "        'source': ['". join("','",@output1 ),"'],\n";
        print CODE "        'target': ['". join("','",@output2 ),"']\n    },\n";
        print PAG "<li> <Link to={'/dashboard/$files'}> ". $name1 . " vs ".  $name2 ."</Link></li>\n";
    }

}
print CODE "    'ancestor-source': {
        'target': ['BN1', 'BN2', 'BN3', 'BN4', 'BN5', 'BN6', 'BN7', 'BN8'],
        'source': ['AN1', 'AN2', 'AN3', 'AN4', 'AN5', 'AN6', 'AN7', 'AN8', 'AN9']
    }
}\n";
close PAG;
close CODE;