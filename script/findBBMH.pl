#!/usr/bin/perl -w

use Getopt::Long;
use FindBin;
use Pod::Usage;

use strict;

my $SEPARATOR = "_";
my $blast_species1 = "";
my $blast_species2 = "";
my $output = ""; 
my $help;
my $usage = $FindBin::Bin ."/". $FindBin::Script.q/ --help

Parameters

    -blast_species1   blast species1 
    -blast_species2   blast species2
    -output 		  output file name 
    -help
/;
GetOptions(
    'blast_species1=s' => \$blast_species1,
    'blast_species2=s' => \$blast_species2,
    'output=s'         => \$output, 
    'help|h|?'         => \$help
)  or pod2usage(-message => $usage);
if ($help) { pod2usage(-message => $usage); }

if ($blast_species1 eq "") {
    warn "\nWarn :: --blast_species1 is empty. Please specify a blast tabulated file for species 1\n\n";
    warn $usage;
}
if ($blast_species2 eq "") {
    warn "\nWarn :: --blast_species2 is empty. Please specify a blast tabulated file for species 2\n\n";
    warn $usage;
}
if ($output eq "") {
    warn "\nWarn :: --output is empty. Please specify a output file name\n\n";
    warn $usage;
}


my @a_files;
push @a_files, $blast_species1, $blast_species2;
chomp(@a_files); 
# List all available blast files:
my (%h_speciesA_speciesB_blastFile) = ();
foreach my $file ( @a_files )	{
	my ($speciesA, $speciesB) = ( $file =~ /([^\/\.]+)$SEPARATOR([^\/\.]+)/ );
	
	if ( !defined($speciesA) || !defined($speciesB) )	{
		print STDERR "ERROR: Cannot parse species names in filename '$file'\n";
		exit(2);
	}
	$h_speciesA_speciesB_blastFile{$speciesA}{$speciesB} = $file;
}
# Check that all blast files were found:
my (@a_species) = (sort keys %h_speciesA_speciesB_blastFile);
my (@a_missingFiles) = ();
foreach my $speciesA ( @a_species )	{
	foreach my $speciesB ( @a_species )	{
		next() if ($speciesA eq $speciesB);
		if ( !defined($h_speciesA_speciesB_blastFile{$speciesA}{$speciesB}) )	{
			push(@a_missingFiles, "$speciesA $SEPARATOR $speciesB");
		}
	}
}
if ( $#a_missingFiles >= 0 )	{
	print STDERR "ERROR: The following blast file(s) is/are missing:\n  - " . join("\n  - ", @a_missingFiles) . "\n";
	exit(3);
}
# Find BBMH for each species
open(OUT,">$output");
foreach my $speciesA ( @a_species )	{
	my (%h_speciesB_seqIDA_BBMHseqIDB) = ();
	&findSpeciesBBMH($speciesA, \%h_speciesA_speciesB_blastFile, \%h_speciesB_seqIDA_BBMHseqIDB);
	my @result = @{&printSpeciesBBMH($speciesA, \%h_speciesB_seqIDA_BBMHseqIDB)};
	print OUT join("\n",@result),"\n";
}
close OUT; 


sub printSpeciesBBMH {
    my ($speciesA, $rh_speciesB_seqIDA_BBMHseqIDB) = @_;
	my @result; 
	my (@a_speciesB) = (sort keys %{$rh_speciesB_seqIDA_BBMHseqIDB});
	my (%h_seqIDA) = ();
	foreach my $speciesB (@a_speciesB) {
		foreach my $seqIDA (keys %{$$rh_speciesB_seqIDA_BBMHseqIDB{$speciesB}}) {
			$h_seqIDA{$seqIDA} = 1;
		}
	} 
	foreach my $seqIDA (keys %h_seqIDA) {
		my $line = "$seqIDA";
		foreach my $speciesB (@a_speciesB) {
			$$rh_speciesB_seqIDA_BBMHseqIDB{$speciesB}{$seqIDA} = "" if ( !defined($$rh_speciesB_seqIDA_BBMHseqIDB{$speciesB}{$seqIDA}) );
			$line .= "\t$$rh_speciesB_seqIDA_BBMHseqIDB{$speciesB}{$seqIDA}";
		} 
		push @result,  $line if ( $line !~ /^$seqIDA\s*$/ );
	} 
	return \@result;
}  # end printSpeciesBBMH



sub findBestHit {
    my ($blastFile, $rh_seqIDA_seqIDB_bestHit) = @_;
	open(BLAST, $blastFile) || die "Cannot open blast file '$blastFile'\n";
	while ( defined(my $line=<BLAST>) ) {
		chomp($line);
		next() if ( ($line =~ /^#/) || ($line !~ /\S/) );
		if ( $line !~ /^(\S+)\t(\S+)\t/ ) {
			print STDERR "ERROR: Cannot parse the following blast line from file '$blastFile':\n#$line#\n";
			exit(4);
		}
		my ($seqIDA, $seqIDB) = ($1, $2);
		if ( !defined($$rh_seqIDA_seqIDB_bestHit{$seqIDA}) ) {
			$$rh_seqIDA_seqIDB_bestHit{$seqIDA} = $seqIDB;
		}
	}
	close(BLAST);
}  # end findBestHit


sub findSpeciesBBMH {
    my ($speciesA, $rh_speciesA_speciesB_blastFile, $rh_speciesB_seqIDA_BBMHseqIDB) = @_;
    my (@a_speciesB) = (sort keys %{$$rh_speciesA_speciesB_blastFile{$speciesA}});
	foreach my $speciesB (@a_speciesB)	{
		my (%h_seqIDA_seqIDB_bestHit) = ();
		&findBestHit($$rh_speciesA_speciesB_blastFile{$speciesA}{$speciesB}, \%h_seqIDA_seqIDB_bestHit);
		
		my (%h_seqIDB_seqIDA_bestHit) = ();
		&findBestHit($$rh_speciesA_speciesB_blastFile{$speciesB}{$speciesA}, \%h_seqIDB_seqIDA_bestHit);
		
		foreach my $seqIDA ( keys %h_seqIDA_seqIDB_bestHit ) {
			my $seqIDB = $h_seqIDA_seqIDB_bestHit{$seqIDA};
			if ( defined($seqIDB) && defined($h_seqIDB_seqIDA_bestHit{$seqIDB}) && ($seqIDA eq $h_seqIDB_seqIDA_bestHit{$seqIDB}) ) {
				$$rh_speciesB_seqIDA_BBMHseqIDB{$speciesB}{$seqIDA} = $seqIDB;
			}
			else {
				$$rh_speciesB_seqIDA_BBMHseqIDB{$speciesB}{$seqIDA} = "";
			}
		}
		
	}
}  # end findSpeciesBBMH

