#!/usr/bin/env python

# Standard library packages
import os

# Import Numpy, Pandas and Seaborn
import numpy as np
import pandas as pd

a = []                  #List a stores the first index headers (db values)
a1 = []                 #List a1 stores the second index headers (query headers)
b = []                  #List b stores the first index headers (db values)
b1 = []
output_list_1 = []
output_list_2 = []      #Inversing the list in order to find common things bw output_list_1 and output_list_2
output_list = []        #Contains only reciprocal blast hits
fwd_out = "fasta/ENSGL_MUSAC.out"
rev_out = "fasta/MUSAC_ENSGL.out"
with open(fwd_out) as fh1:
    for line in fh1.readlines():
        a.append(line.split()[0])
        a1.append(line.split()[1])

for i,j in zip(a,a1):
    output_list_1.append(i+"\t"+j+"\n")

with open(rev_out) as fh2:
    for line in fh2.readlines():
        b.append(line.split()[0])
        b1.append(line.split()[1])

for i,j in zip(b,b1):
    output_list_2.append(j+"\t"+i+"\n")

y = None
for x in output_list_1:         #Checking common hits beween output_list_1 and output_list_2
    if x in output_list_2:
        if y is not None:
            if x.split()[0] == y:
                next
            else:
                output_list.append(x)   #Contains only reciprocal blast hits
                y = x.split()[0]
        else:
            output_list.append(x)   #Contains only reciprocal blast hits
            y = x.split()[0]
            
with open("output_file", 'w') as output_fh:
        for ortholog_pair in output_list:
            output_fh.write(ortholog_pair)
# 
# # Load the BLAST results into Pandas dataframes
# fwd_results = pd.read_csv(fwd_out, sep="\t", header=None)
# rev_results = pd.read_csv(rev_out, sep="\t", header=None)
# 
# # Add headers to forward and reverse results dataframes
# headers = ["query", "subject", "identity", "coverage",
#            "qlength", "slength", "alength",
#            "bitscore", "E-value"]
# fwd_results.columns = headers
# rev_results.columns = headers
# rbbh = pd.merge(fwd_results, rev_results[['query', 'subject']],
#                 left_on='subject', right_on='query',
#                 how='inner')
# 
# # Discard rows that are not RBH
# rbbh = rbbh.loc[rbbh.query_x == rbbh.subject_y]
# # Group duplicate RBH rows, taking the maximum value in each column
# rbbh = rbbh.groupby(['query_x', 'subject_x']).max()
# 
# 
#  
# with open("mybbmh",'w') as outfile:
#     rbbh.to_csv(outfile,sep="\t")
# 

