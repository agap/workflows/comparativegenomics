import itertools
import snakemake
import random
import os
import shutil
import sys
configfile: "config.yaml"

from itertools import combinations

# Globals ---------------------------------------------------------------------
input_proteome=config["input_proteome"]


list_proteome=input_proteome.keys()

list_permutations = ['_'.join(pair) for pair in itertools.permutations(list_proteome, 2)]
list_combinations = ['_'.join(pair) for pair in itertools.combinations(list_proteome, 2)]

wildcard_constraints:
    database = "|".join(input_proteome.keys())
# Here, the first attempt will require 4096 MB memory, the second attempt will require 9192 MB memory and so on.
def get_mem_mb(wildcards, attempt):
    return attempt * 9192

def get_mem_liftoff(wildcards, threads):
    return min(threads * 4096, 16384)

def generate_color():
    color = '#{:02x}{:02x}{:02x}'.format(*map(lambda x: random.randint(0, 255), range(3)))
    return color

def get_mem_liftoff(wildcards, threads):
    return threads * 4096


  
rule all:
    input:
        expand("results/jbrowse/{permutation}/{permutation}.gff3.gz",permutation=list_permutations,database=list_proteome), 
        expand("results/jbrowse/{permutation}/{permutation}.conf",permutation=list_permutations,database=list_proteome),
        expand("results/synvisio/{permutation}_collinear.collinearity",permutation=list_permutations),
        expand("results/liftoff/{permutation}/{permutation}_mcscan.gff3.gz",permutation=list_permutations),
        "results/synvisio/full_comparison_collinear.collinearity",
        "results/synvisio/Home.jsx",
     #   "results/orthofinder/orthofinder.done"


# cp gff3 for liftoft usage 
rule get_data:
    input:
        lambda wildcards: input_proteome[wildcards.second]["gff3"]
    output:
        "results/liftoff/{first}_{second}/{second}.gff3"
    shell:
        "cp {input} {output}"

# Index Genome      
rule index_genome:
    input:
        lambda wildcards: input_proteome[wildcards.first]["genome"]
    output:
        fai = "{first}/{first}.fai",
        fasta = "{first}/{first}.fasta"      
    envmodules:
        config["modules"]["samtools"]
    singularity:
        config["containers"]["samtools"]

    shell:"""
        ln -nrfs {input} {output.fasta};
        samtools faidx {input} -o {output.fai};
    """


# Transfert annotation using liftoff from genome2 to genome1     
    
rule liftoff:
    input: 
        gff = rules.get_data.output, 
        genome1 = "{first}/{first}.fasta",
        genome2 = "{second}/{second}.fasta"
    output:
        gff3 =  "results/liftoff/{first}_{second}/{first}_{second}_unsorted.gff3",
        db = "results/liftoff/{first}_{second}/{second}.gff3_db",
        sam = "results/liftoff/{first}_{second}/reference_all_to_target_all.sam",
        fa = "results/liftoff/{first}_{second}/reference_all_genes.fa"
    params:
        directory = "results/liftoff/{first}_{second}",
        coverage = config["liftoff"]["coverage"]
    threads:
        config["threads"]
    envmodules:
        config["modules"]["liftoff"]
    singularity:
        config["containers"]["liftoff"]
    shell:"""
        liftoff -dir {params.directory} -a {params.coverage} -o {output.gff3} -p {threads} -copies -g {input.gff} {input.genome1} {input.genome2}
    """

# GFF3 need to be properly sorted for indexing with tabix
rule gff3sort:
    input:
        rules.liftoff.output.gff3
    output:
        temp("results/jbrowse/{first}_{second}/{first}_{second}.gff3")
    envmodules:
        config["modules"]["perllib"]
    singularity:
        config["containers"]["perllib"]
    shell:"""
        script/gff3sort.pl --precise {input} > {output}
    """

# Indexing GFF before integration on JBrowse
rule tabix:
    input:
        rules.gff3sort.output
    output:
        gz = "results/jbrowse/{first}_{second}/{first}_{second}.gff3.gz",
        tbi = "results/jbrowse/{first}_{second}/{first}_{second}.gff3.gz.tbi"
    envmodules:
        config["modules"]["tabix"]
    singularity:
        config["containers"]["tabix"]
    params:
        jbrowse_dir = lambda wildcards: input_proteome[wildcards.first]["jbrowse"] + "/liftoff" 
    shell:"""
       bgzip -c {input} > {output.gz};
       tabix -p gff {output.gz};
       mkdir -p {params.jbrowse_dir};
       cp {output.tbi} {output.gz} {params.jbrowse_dir}
    """

# Write tracks config to integrate in JBrowse
rule write_jbrowse_config:
    input:
        gff3 = rules.tabix.output.gz,
        cfg=config["template"]
    params:
        track_name = "{first}_{second}",
        jbrowse_dir  = "liftoff",
        color = generate_color(),
        category = config["category"],
        genome1 = lambda wildcards: input_proteome[wildcards.first]["name"],
        genome2 = lambda wildcards: input_proteome[wildcards.second]["name"],
        jbrowse_path = lambda wildcards: input_proteome[wildcards.first]["jbrowse"] + "/liftoff",
        jbrowse_instance = lambda wildcards: input_proteome[wildcards.second]["jbrowse_url"]
    output:
        "results/jbrowse/{first}_{second}/{first}_{second}.conf" 
    run:
        from string import Template
        d = {
            'track_name': params[0],
            'real_name': params[5],
            'gff3': os.path.basename(input[0]),
            'color': params[2],
            'category': params[3],
            'jbrowse_instance': params[7]
        }
        file = os.path.join(params[6],params[0]+".conf")
        fh_out = open(output[0], "w")
        fh2_out = open(file,"w")
        with open(input[1], 'r') as f:
            src = Template(f.read())
            result = src.substitute(d)
            fh_out.write(result)
            fh2_out.write(result)
       # shutil.copyfile(output[0], params[6])

rule make_data:
    input:
        gff = lambda wildcards: input_proteome[wildcards.database]["gff3"],
        fai  = "{database}/{database}.fai",
        fasta = lambda wildcards: input_proteome[wildcards.database]["fasta"]
    output:
        fasta = "{database}/{database}.faa",
        gff1  = "{database}/{database}.gff",
        gff2  = "{database}/{database}_synvisio.gff",
        bed   = "{database}/{database}.bed",
        coord = "{database}/{database}_chromosome.bed" 
    params:
        outdir = "{database}",
        prefix = lambda wildcards: input_proteome[wildcards.database]["prefix"],
        level = lambda wildcards: input_proteome[wildcards.database]["level"],
        length_cutoff = config["length_cutoff"]
    envmodules:
        config["modules"]["perllib"] 
    singularity:
        config["containers"]["perllib"]
    shell:"""
        script/cnv_gff.pl --gff {input.gff} --assembly {input.fai} --fasta {input.fasta} --prefix {params.prefix} --outdir {params.outdir} --is_contig {params.level} --length {params.length_cutoff}
    """
    
rule diamond_prepdb:
    input:
        rules.make_data.output.fasta
    output:
        "{database}/{database}.faa.dmnd"
    envmodules:
        config["modules"]["diamond"]
    singularity:
        config["containers"]["diamond"]
    shell:"""
        diamond makedb --in {input} -d {input}
    """

# Run diamond for each combination
rule diamond_analysis:
    input:
        fasta = "{first}/{first}.faa",
        db    = "{second}/{second}.faa",
        dmd   = "{second}/{second}.faa.dmnd"
    output:
        "results/liftoff/{second}_{first}/{second}_{first}.out"
    envmodules:
        config["modules"]["diamond"]
    singularity:
        config["containers"]["diamond"]
    params:
        evalue=1e-10,
        max_target_seqs=2,
        mode="--very-sensitive",
        output_format="--outfmt 6 qseqid sseqid pident qcovhsp qlen slen length bitscore evalue"
    threads:
        config["threads"]
    shell:"""
        diamond blastp  --masking 0 --unal 1 {params.output_format}  {params.mode} --threads {threads}  --db {input.db} --query {input.fasta} --evalue {params.evalue} -k {params.max_target_seqs}  -o {output}
    """


# Parsing diamond output to find Best mutual blast
rule bbmh:
    input:
        blast1 = "results/liftoff/{first}_{second}/{first}_{second}.out",
        blast2 = "results/liftoff/{second}_{first}/{second}_{first}.out"
    output:
        synvisio = "results/liftoff/{first}_{second}/{first}_{second}.homology",
        jbrowse = "results/liftoff/{first}_{second}/{first}_{second}_jbrowse.homology",
        
    params:
        is_chromosome_first = lambda wildcards: input_proteome[wildcards.first]["level"],
        is_chromosome_second = lambda wildcards: input_proteome[wildcards.second]["level"],
        module = config["modules"]["perllib"] 
    run:
        if params.is_chromosome_first == 'contig':
            shell("touch {output}")
        elif params.is_chromosome_second == 'contig':
            shell("touch {output}")
        else:
            shell("module load {params.module};script/findBBMH.pl  -blast_species1 {input.blast1} -blast_species2 {input.blast2} -output {output.synvisio}; cp {output.synvisio} {output.jbrowse}  ")
        
    
rule cat_bed:
    input:
        bed1 = "{first}/{first}_synvisio.gff",
        bed2 = "{second}/{second}_synvisio.gff",
        bed3 = "{first}/{first}.gff",
        bed4 = "{second}/{second}.gff",
        
    output:
        gff1 = "results/liftoff/{first}_{second}/{first}_{second}.gff",
        gff2 = "results/liftoff/{first}_{second}/{first}_{second}_jbrowse.gff",
        synvisio = "results/synvisio/{first}_{second}_coordinate.gff"
    params:
        is_chromosome_first = lambda wildcards: input_proteome[wildcards.first]["level"],
        is_chromosome_second = lambda wildcards: input_proteome[wildcards.second]["level"] 
    run:
        if params.is_chromosome_first == 'contig':
            shell("touch {output.gff1} {output.synvisio}")
        elif params.is_chromosome_second == 'contig':
            shell("touch {output.gff1} {output.synvisio}")
        else:
            shell("cat {input.bed1} {input.bed2} > {output.gff1};cat {input.bed3} {input.bed4} > {output.gff2};cat {input.bed1} {input.bed2} > {output.synvisio}; ")
        
    
rule mcscanx:
    input:
        gff = rules.cat_bed.output.gff1,
        homology = rules.bbmh.output.synvisio
    output:
        collinearity = "results/liftoff/{first}_{second}/{first}_{second}.collinearity",
        synvisio = "results/synvisio/{first}_{second}_collinear.collinearity"
    params:
        directory = "results/liftoff/{first}_{second}/{first}_{second}",
        is_chromosome_first = lambda wildcards: input_proteome[wildcards.first]["level"],
        is_chromosome_second = lambda wildcards: input_proteome[wildcards.second]["level"],
        size_threshold = 400
    envmodules:
        config["modules"]["mcscanx"] 
    shell:"""
        MCScanX_h -a {params.directory}
        file_size=$(stat -c %s {output.collinearity})
        if [ "$file_size" -lt {params.size_threshold} ]; then
            MCScanX_h -b 1 -a {params.directory}
        fi
        cp {output.collinearity} {output.synvisio}
    """
 

rule mcscanx2:
    input:
        gff = rules.cat_bed.output.gff2,
        homology = rules.bbmh.output.jbrowse
    output:
        collinearity = "results/liftoff/{first}_{second}/{first}_{second}_jbrowse.collinearity"
    params:
        directory = "results/liftoff/{first}_{second}/{first}_{second}_jbrowse",
        is_chromosome_first = lambda wildcards: input_proteome[wildcards.first]["level"],
        is_chromosome_second = lambda wildcards: input_proteome[wildcards.second]["level"],
        size_threshold = 400
    envmodules:
        config["modules"]["mcscanx"] 
    shell:"""
        MCScanX_h -a {params.directory}
        file_size=$(stat -c %s {output.collinearity})
        if [ "$file_size" -lt {params.size_threshold} ]; then
            MCScanX_h -b 1 -a {params.directory}
        fi
    """
 


rule format_mcscan:
    input:
        collinearity = "results/liftoff/{first}_{second}/{first}_{second}_jbrowse.collinearity",
        gff = "results/liftoff/{first}_{second}/{first}_{second}_jbrowse.gff"
    output:
        temp = "results/liftoff/{first}_{second}/{first}_{second}_unsorted_mcscan.gff3",
        gff3 = "results/liftoff/{first}_{second}/{first}_{second}_mcscan.gff3",
        gz = "results/liftoff/{first}_{second}/{first}_{second}_mcscan.gff3.gz",
        tbi = "results/liftoff/{first}_{second}/{first}_{second}_mcscan.gff3.gz.tbi"
    params:
        prefix = "{first}_{second}",
        jbrowse_dir = lambda wildcards: input_proteome[wildcards.first]["jbrowse"] + "/mcscan" 
    envmodules:
        config["modules"]["perllib"] ,
        config["modules"]["tabix"]   
    shell:"""
        script/format_jbrowse.pl --coordinate {input.gff} --collinearity {input.collinearity}  -prefix {params.prefix} -out {output.temp};
        script/gff3sort.pl --precise {output.temp} > {output.gff3};
        bgzip -c {output.gff3} > {output.gz};
        tabix -p gff {output.gz};
        mkdir -p {params.jbrowse_dir};
        cp {output.gz} {output.tbi} {params.jbrowse_dir}
    """
    

rule bedops:
    input:
        coord = rules.make_data.output.coord,
        bed   = rules.make_data.output.bed
    output:
        "{database}/{database}_gene_count.txt"  
    envmodules:
        config["modules"]["bedops"]   
    singularity:
        config["containers"]["bedops"]     
    shell:"""
        bedops --chop 200000 {input.coord} | bedmap --echo --count - {input.bed} > {output};
        sed -i 's:|:\t:' {output}
    """

rule cat_gene_count:
    input:
        bed1 = "{first}/{first}_gene_count.txt",
        bed2 = "{second}/{second}_gene_count.txt"
    output:
        "results/synvisio/{first}_{second}_gene_count.txt" 
    params:
        is_chromosome_first = lambda wildcards: input_proteome[wildcards.first]["level"],
        is_chromosome_second = lambda wildcards: input_proteome[wildcards.second]["level"]
    run:
        if params.is_chromosome_first == 'contig':
            shell("touch {output}")
        elif params.is_chromosome_second == 'contig':
            shell("touch {output}")
        else:
            shell("cat {input} > {output}; ")

rule merge_input:
    input:
        homology = expand("results/liftoff/{permutation}/{permutation}.homology",permutation=list_permutations),
        gene_count = expand("results/synvisio/{permutation}_gene_count.txt",permutation=list_permutations),
        gff = expand("results/liftoff/{permutation}/{permutation}.gff",permutation=list_permutations)
    output:
        homology = temp("results/synvisio/full_comparison.homology"),
        gene_count = "results/synvisio/full_comparison_gene_count.txt",
        gff = temp("results/synvisio/full_comparison.gff"),
        gff_synvisio = "results/synvisio/full_comparison_coordinate.gff" 
    envmodules:
        config["modules"]["mcscanx"]
    shell:"""
        cat {input.homology} | sort -u > {output.homology};
        cat {input.gene_count} | sort -u > {output.gene_count};
        cat {input.gff} | sort -u > {output.gff};
        cat {input.gff} | sort -u > {output.gff_synvisio};
    """
rule mcscanx_complete:
    input:
        gff = rules.merge_input.output.gff,
        homology = rules.merge_input.output.homology
    output:
        collinearity = temp("results/synvisio/full_comparison.collinearity"),
        synvisio = "results/synvisio/full_comparison_collinear.collinearity"
    envmodules:
        config["modules"]["mcscanx"]
    params:
        directory = "results/synvisio/full_comparison",
        size_threshold = 400
    shell:"""
        MCScanX_h -a {params.directory}
        file_size=$(stat -c %s {output.collinearity})
        if [ "$file_size" -lt {params.size_threshold} ]; then
            MCScanX_h -b 1 -a {params.directory}
        fi
        cp {output.collinearity} {output.synvisio}
    """

rule advanced_synvisio:
    input: 
        yaml = "config.yaml",
        synvisio = "results/synvisio/full_comparison_collinear.collinearity"
    output:
        page = "results/synvisio/Home.jsx",
        sample = "results/synvisio/sampleSourceMapper.js"
    params:
        directory = "results/synvisio"         
    envmodules:
        config["modules"]["perllib"]
    singularity:
        config["containers"]["perllib"]
    shell:"""
        script/format_synvisio.pl --directory {params.directory} --config {input.yaml}
    """
    
rule orthofinder:
    input:
        expand("{database}/{database}.faa", database=list_proteome)
    output:
        touch("results/orthofinder/orthofinder.done")
    envmodules:
        config["modules"]["orthofinder"]
    singularity:
        config["containers"]["orthofinder"]
    threads:
        config["threads"]
    params:
        directory = "results/orthofinder"
    shell:"""
        mkdir -p results/orthofinder;
        cp {input} results/orthofinder;
        orthofinder -t {threads} -a {threads} -M msa -T fasttree -X -S diamond -f {params.directory} -n orthofinder;
    """     
