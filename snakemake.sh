#!/bin/bash 
#
#SBATCH -J comparative
#SBATCH -o comparative."%j".out
#SBATCH -e comparative."%j".err 

# Partition name
#SBATCH -p agap_long

module purge

module load snakemake/7.15.1-conda
 
mkdir -p logs/ 

### Snakemake commands
# Dry run
if [ "$1" = "dry" ]
then
    snakemake --profile profile  --jobs 200 --printshellcmds --dryrun --use-envmodules --cores 1
elif [ "$1" = "unlock" ]
then
    # Unlock repository if one job failed
    snakemake   --profile profile --jobs 200 --unlock  --use-envmodules
elif [ "$1" = "dag" ]
then
    # Create DAG file
    snakemake  --profile profile --jobs 2 --dag  --use-envmodules | dot -Tpng > dag.png
else
    # Run workflow
    snakemake --profile profile --jobs 60 --cores 140 -p --use-envmodules --latency-wait 120
fi
