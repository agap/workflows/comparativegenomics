# Snakemake workflow: Comparative Genomics


[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)    
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

**Table of Contents**
 
  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Overview of programmed workflow](#overview-of-programmed-workflow)
  - [Procedure](#procedure)
  - [Output directory](#output-directory)
 
## Objective

This is a project that aims to build a computational pipeline to extract genome synteny relationships, including predicting orthogroups
The pipeline takes as input the protein sequences and annotation gff file of multiple genomes and does the following steps:
* Prepare input data: based on the gff files, we find for each gene its longest protein isoform; so that we have a 1 to 1 mapping between geneID and proteinID. We also extract gene position for each gene.
* Build orthogroups and species tree using **OrthoFinder** on the selected (i.e. longest) protein sequences to detect orthogroups and build species tree.
* Build synteny blocks using **MCScanX** which takes as input the reciprocal blast hit, and gene positions.
* Align the gene sequences from a reference genome to the target genome using **Liftoff**. 

## Dependencies

* diamond : DIAMOND is a sequence aligner for protein and translated DNA searches, designed for high performance analysis of big sequence data. (https://github.com/bbuchfink/diamond) 
* bedops : The fast, highly scalable and easily-parallelizable genome analysis toolkit (hhttps://bedops.readthedocs.io/en/latest/)
* mcscanx : detects syntenic blocks and progressively aligns multiple syntenic blocks against reference genomes (https://github.com/wyp1125/MCScanX)
* tabix : Generic indexer for TAB-delimited genome position files (http://www.htslib.org/doc/tabix.html)
* liftoff : Liftoff is a tool that accurately maps annotations in GFF or GTF between assemblies of the same, or closely-related species. (https://github.com/agshumate/Liftoff)
* orthofinder : phylogenetic orthology inference for comparative genomics (https://github.com/davidemms/OrthoFinder)
## Overview of programmed workflow

<p align="center">
<img src="images/dag.png" width="50%">
</p>


## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
  ```
  git clone https://gitlab.cirad.fr/umr_agap/workflows/ComparativeGenomics.git
  ```


- Change directories into the cloned repository:
  ```
  cd ComparativeGenomics
  ```

- Edit the configuration file config.yaml

```
# Genome to be annotated (Assembly, GFF3 and corresponding protein fasta file)


# template : will allow to write the config for Jbrowse with the rule write_jbrowse_config
template: "template/jbrowse.conf"
# category = To include in the Jbrowse config file to classify tracks in a specific section
category: "3. Comparative Genomics"

# input_proteome = For each organism define by the first keys (i.e ENSGL), you need to provide several file or value
# - fasta = Translated nucleotide sequence as fasta
# - genome = Assembly as fasta
# - gff3 = Annotation file in GFF format, for each mRNA, we used the tag value Name which should be same as fasta file 
# - jbrowse = JBrowse Path on muse
# - jbrowse_url = JBrowse URL   
# - name = Species name
# - prefix = Due to a technical constraint of Synvisio, chromosome id need to be fixed to only 2 letters
# - level = contig or chromosome (Synvisio run only at chromosome scale)

input_proteome:
  "CITMA":
    "fasta": "/storage/replicated/cirad/web/HUBs/citrus/citrus_maxima/CITMA_protein.faa"
    "genome": "/storage/replicated/cirad/web/HUBs/citrus/citrus_maxima/CITMA.fasta"
    "gff3": "/storage/replicated/cirad/web/HUBs/citrus/citrus_maxima/CITMA.gff3" 
    "jbrowse": "/storage/replicated/cirad/web/HUBs/citrus/citrus_maxima"
    "jbrowse_url": "https://citrus-genome-hub.southgreen.fr/citrus_maxima"
    "name": "C. maxima"
    "prefix": "ma"
    "level": "chromosome"
  "CITMI":
    "fasta": "/storage/replicated/cirad/web/HUBs/citrus/citrus_micrantha/CITMI_protein.faa"
    "genome": "/storage/replicated/cirad/web/HUBs/citrus/citrus_micrantha/CITMI.fasta"
    "gff3": "/storage/replicated/cirad/web/HUBs/citrus/citrus_micrantha/CITMI.gff3" 
    "jbrowse": "/storage/replicated/cirad/web/HUBs/citrus/citrus_micrantha"
    "jbrowse_url": "https://citrus-genome-hub.southgreen.fr/citrus_micrantha"
    "name": "C. micrantha"
    "prefix": "mi"
    "level": "chromosome"

  
threads: 4
modules:
  liftoff: "liftoff/1.6.3"
  perllib: "perllib"
  tabix: "tabix/0.2.6"
  ncbi-blast: "ncbi-blast/2.12.0+"
  diamond: "diamond/2.0.6"
  bedops: "bedops/2.4.39"
  mcscanx: "mcscanx/2"
  samtools: "samtools/1.14-bin"
  orthofinder: "orthofinder/2.5.4-bin"

```

- Load Snakemake (Specific to our cluster)
```bash
module load snakemake/7.15.1-conda
```

- Print shell commands to validate your modification (Dry run)

```bash
sbatch snakemake.sh dry 
```

- Run workflow on Meso@LR

```bash
sbatch snakemake.sh run
```

- Unlock directory
```bash
sbatch snakemake.sh unlock
```
 
- Create DAG file from workflow

```bash
sbatch snakemake.sh dag
```
        
## Output directory : 


* **Synvisio Results files** : results/synvisio
  - Contains all permutations for each organism define by the first keys in the input_proteome parameter + One who combine all permutation
  - Home.jsx and sampleSourceMapper.js are used to compile Synvisio. This file correspond to that provide in Synvisio github /src/pages and src/utils
  
  A tutorial presents how to integrate these results into a Synvisio instance (https://gitlab.cirad.fr/droc/genomehub/-/wikis/Set-up-a-new-Synvisio-Instance)

* **Liftoft Results files** : results/liftoft 
  - Contains all permutations for each organism define by the first keys in the input_proteome parameter (i.e ENSGL_MABAN)
  - This files can be include on Jbrowse following this [procedure](https://gitlab.cirad.fr/droc/genomehub/-/wikis/Create-a-new-Jbrowse-instance)

* **Orthofinder Results files** : results/orthofinder/

